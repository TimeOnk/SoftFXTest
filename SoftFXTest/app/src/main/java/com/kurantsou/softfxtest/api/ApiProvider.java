package com.kurantsou.softfxtest.api;

import io.reactivex.Observable;
import me.toptas.rssconverter.RssConverterFactory;
import me.toptas.rssconverter.RssFeed;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.http.GET;

/**
 * Created by artem on 02.09.2017.
 */

public class ApiProvider {

    private static RssFeedApi mApi;

    public static RssFeedApi getApiInstance() {
        if (mApi == null)
            buildApi();
        return mApi;
    }

    private static void buildApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUtils.API_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(RssConverterFactory.create())
                .client(createClient())
                .build();
        mApi = retrofit.create(RssFeedApi.class);
    }

    private static OkHttpClient createClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        return client;
    }

    public interface RssFeedApi {

        @GET("/GetLiveNewsRss")
        Observable<RssFeed> getNewsFeed();

        @GET("/GetAnalyticsRss")
        Observable<RssFeed> getAnalyticsFeed();

    }
}
