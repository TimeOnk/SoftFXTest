package com.kurantsou.softfxtest.db;

/**
 * Created by artem on 02.09.2017.
 */

public class DbConstants {
    public static final int SCHEMA_VERSION = 1;
    public static final String DB_NAME = "softFXTestDb.sqlite";
    public static final int MAX_ITEMS_COUNT = 100;

    public static abstract class TypesEntry{
        public static final String NEWS_TYPE_NAME = "News";
        public static final String ANALYTICS_TYPE_NAME = "Analytics";

        public static final String TABLE_NAME = "Types";
        public static final String COLUMN_ID_NAME = "_id";
        public static final String COLUMN_NAME_NAME = "name";
    }

    public static  abstract class ItemsEntry {
        public static final String TABLE_NAME = "Items";
        public static final String COLUMN_ID_NAME = "_id";
        public static final String COLUMN_TITLE_NAME = "title";
        public static final String COLUMN_DESCRIPTION_NAME = "description";
        public static final String COLUMN_DATE_NAME = "date";
        public static final String COLUMN_TYPE_ID_NAME = "type_id";
    }
}
