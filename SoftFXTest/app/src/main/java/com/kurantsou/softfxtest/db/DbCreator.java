package com.kurantsou.softfxtest.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by artem on 02.09.2017.
 */

public class DbCreator {

    public static void createDb(SQLiteDatabase sqLiteDatabase) {

        //creating tables
        sqLiteDatabase.execSQL(DbQueries.CREATE_TYPE_TABLE_QUERY);
        sqLiteDatabase.execSQL(DbQueries.CREATE_ITEM_TABLE_QUERY);

        //init types table
        sqLiteDatabase.execSQL(DbQueries.INIT_TYPES_TABLE_QUERY);
    }
}
