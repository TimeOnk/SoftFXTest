package com.kurantsou.softfxtest.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.kurantsou.softfxtest.model.Item;

import java.util.List;

/**
 * Created by artem on 03.09.2017.
 */

public class DbInserter {

    private static final String TAG = "DbInserter";

    public static void insertItems(Context context, List<Item> items, String itemsTypeName){
        ItemsDbHelper mDbHelper = new ItemsDbHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        int itemsDeleted = deleteOldItemsIfNeeded(db, items.size());
        Log.d(TAG, "deletedItems: = " + itemsDeleted);

        long itemsTypeId = -1;
        Cursor cursor = db.query(DbConstants.TypesEntry.TABLE_NAME,
                new String[]{DbConstants.TypesEntry.COLUMN_ID_NAME},
                DbConstants.TypesEntry.COLUMN_NAME_NAME + " = ?",
                new String[]{itemsTypeName},
                null, null, null);
        if (cursor.moveToFirst())
            itemsTypeId = cursor.getLong(cursor.getColumnIndex(DbConstants.TypesEntry.COLUMN_ID_NAME));

        cursor.close();

        int itemsInserted = 0;

        for (Item item : items)
            itemsInserted += insertItemIfNotExist(db, item, itemsTypeId);
        Log.d(TAG, "insertItems: inserted = " + itemsInserted);
    }

    private static int deleteOldItemsIfNeeded(SQLiteDatabase db, int countItemsToAdd) {
        long itemsToDelete = DatabaseUtils.queryNumEntries(db, DbConstants.ItemsEntry.TABLE_NAME) + countItemsToAdd - DbConstants.MAX_ITEMS_COUNT;
        int itemsDeleted = 0;
        if (itemsToDelete > 0) {
            long[] idsToDel = new long[(int)itemsToDelete];
            Cursor cursor = db.query(DbConstants.ItemsEntry.TABLE_NAME,
                    new String[]{DbConstants.ItemsEntry.COLUMN_ID_NAME},
                    null,
                    null,
                    null,
                    null,
                    DbConstants.ItemsEntry.COLUMN_DATE_NAME + " ASC");
            if (cursor.moveToFirst()){
                int i = 0;
                int idColumnIndex = cursor.getColumnIndex(DbConstants.ItemsEntry.COLUMN_ID_NAME);
                do{
                    idsToDel[i] = cursor.getLong(idColumnIndex);
                    i++;
                    itemsToDelete--;
                }while (itemsToDelete > 0 && cursor.moveToNext());
            }
            cursor.close();
            for (long id : idsToDel)
                itemsDeleted += db.delete(DbConstants.ItemsEntry.TABLE_NAME,
                        DbConstants.ItemsEntry.COLUMN_ID_NAME + " = ?",
                        new String[]{String.valueOf(id)});
        }
        return itemsDeleted;
    }

    private static int insertItemIfNotExist(SQLiteDatabase db, Item item, long itemTypeId) {

        if (checkItemAlreadyExist(db, item, itemTypeId))
            return 0;

        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.ItemsEntry.COLUMN_TITLE_NAME, item.getTitle());
        contentValues.put(DbConstants.ItemsEntry.COLUMN_DESCRIPTION_NAME, item.getDescription());
        contentValues.put(DbConstants.ItemsEntry.COLUMN_DATE_NAME, item.getDate());
        contentValues.put(DbConstants.ItemsEntry.COLUMN_TYPE_ID_NAME, itemTypeId);
        int inserted = db.insert(DbConstants.ItemsEntry.TABLE_NAME, null, contentValues) > 0 ? 1 : 0;
        contentValues.clear();
        return inserted;
    }

    private static boolean checkItemAlreadyExist(SQLiteDatabase db, Item item, long itemTypeId) {
        Cursor cursor = db.query(DbConstants.ItemsEntry.TABLE_NAME,
                new String[]{DbConstants.ItemsEntry.COLUMN_ID_NAME},
                DbConstants.ItemsEntry.COLUMN_TITLE_NAME + " = ? AND " +
                        DbConstants.ItemsEntry.COLUMN_DESCRIPTION_NAME + " = ? AND " +
                        DbConstants.ItemsEntry.COLUMN_DATE_NAME + " = ? AND " +
                        DbConstants.ItemsEntry.COLUMN_TYPE_ID_NAME + " = ?",
                new String[]{item.getTitle(),
                        item.getDescription(),
                        String.valueOf(item.getDate()),
                        String.valueOf(itemTypeId)},
                null, null, null);
        boolean isHasRows = cursor.moveToFirst();
        cursor.close();
        return isHasRows;
    }
}
