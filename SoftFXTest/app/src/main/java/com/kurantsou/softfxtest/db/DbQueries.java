package com.kurantsou.softfxtest.db;

/**
 * Created by artem on 03.09.2017.
 */

public class DbQueries {
    public static final String CREATE_TYPE_TABLE_QUERY = "CREATE TABLE " + DbConstants.TypesEntry.TABLE_NAME + " ( " +
            DbConstants.TypesEntry.COLUMN_ID_NAME + " INTEGER PRIMARY KEY, " +
            DbConstants.TypesEntry.COLUMN_NAME_NAME + " TEXT NOT NULL" +
            ")";
    public static final String CREATE_ITEM_TABLE_QUERY = "CREATE TABLE " + DbConstants.ItemsEntry.TABLE_NAME + " ( " +
            DbConstants.ItemsEntry.COLUMN_ID_NAME + " INTEGER PRIMARY KEY, " +
            DbConstants.ItemsEntry.COLUMN_TITLE_NAME + " TEXT NOT NULL, " +
            DbConstants.ItemsEntry.COLUMN_DESCRIPTION_NAME + " TEXT NOT NULL, " +
            DbConstants.ItemsEntry.COLUMN_DATE_NAME + " INTEGER NOT NULL, " +
            DbConstants.ItemsEntry.COLUMN_TYPE_ID_NAME + " INTEGER NOT NULL" +
            ")";
    public static final String INIT_TYPES_TABLE_QUERY = "INSERT INTO " + DbConstants.TypesEntry.TABLE_NAME + "(" +
            DbConstants.TypesEntry.COLUMN_NAME_NAME + ") VALUES ('" +
            DbConstants.TypesEntry.NEWS_TYPE_NAME + "'),('" +
            DbConstants.TypesEntry.ANALYTICS_TYPE_NAME +
            "')";
}
