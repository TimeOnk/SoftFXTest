package com.kurantsou.softfxtest.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kurantsou.softfxtest.model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artem on 02.09.2017.
 */

public class ItemsDbHelper extends SQLiteOpenHelper {
    
    private static final String TAG = "ItemsDbHelper";

    public ItemsDbHelper(Context context) {
        super(context, DbConstants.DB_NAME, null, DbConstants.SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG, "onCreate: creating db");
        DbCreator.createDb(sqLiteDatabase);
        Log.d(TAG, "onCreate: db created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DbConstants.TypesEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DbConstants.ItemsEntry.TABLE_NAME);

        onCreate(sqLiteDatabase);
    }

    public List<Item> getItems(String itemsTypeName, int limit, int offset){
        SQLiteDatabase db = getReadableDatabase();
        long itemsTypeId = getTypeId(itemsTypeName);
        Cursor cursor = db.query(DbConstants.ItemsEntry.TABLE_NAME,
                new String[]{DbConstants.ItemsEntry.COLUMN_TITLE_NAME, DbConstants.ItemsEntry.COLUMN_DESCRIPTION_NAME, DbConstants.ItemsEntry.COLUMN_DATE_NAME},
                DbConstants.ItemsEntry.COLUMN_TYPE_ID_NAME + " = ?",
                new String[]{String.valueOf(itemsTypeId)},
                null,
                null,
                DbConstants.ItemsEntry.COLUMN_DATE_NAME + " DESC",
                offset + "," + limit);
        ArrayList<Item> items = new ArrayList<>();
        if (cursor.moveToFirst()){
            int titleIndex = cursor.getColumnIndex(DbConstants.ItemsEntry.COLUMN_TITLE_NAME),
                    descriptionIndex = cursor.getColumnIndex(DbConstants.ItemsEntry.COLUMN_DESCRIPTION_NAME),
                    dateIndex = cursor.getColumnIndex(DbConstants.ItemsEntry.COLUMN_DATE_NAME);
            do{
                items.add(new Item(cursor.getString(titleIndex), cursor.getString(descriptionIndex), cursor.getLong(dateIndex)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return items;
    }
    public long getTypeId(String typeName){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DbConstants.TypesEntry.TABLE_NAME,
                new String[]{DbConstants.TypesEntry.COLUMN_ID_NAME},
                DbConstants.TypesEntry.COLUMN_NAME_NAME + " = ?",
                new String[]{typeName}, null, null, null);
        long itemsTypeId = -1;
        if (cursor.moveToFirst())
            itemsTypeId = cursor.getLong(cursor.getColumnIndex(DbConstants.TypesEntry.COLUMN_ID_NAME));
        cursor.close();
        return itemsTypeId;
    }
}
