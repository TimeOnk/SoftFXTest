package com.kurantsou.softfxtest.model;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;


/**
 * Created by artem on 01.09.2017.
 */

public class Item {

    private static final int SHORT_DESCRIPTION_LENGTH = 250;

    private String title;
    private String description;
    private long date;

    public Item() {
    }

    public Item(String title, String description, long date) {
        this.title = title;
        this.description = description;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription(){
        if (description != null && description.length() > SHORT_DESCRIPTION_LENGTH)
            return description.substring(0, SHORT_DESCRIPTION_LENGTH) + "...";
        return description;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getFormattedDate(){
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern("dd.MM.yyyy HH:mm")
                .toFormatter();
        return formatter.print(date);
    }
}
