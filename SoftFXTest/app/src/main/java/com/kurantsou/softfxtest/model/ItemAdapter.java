package com.kurantsou.softfxtest.model;

import android.text.Html;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import java.util.ArrayList;
import java.util.List;

import me.toptas.rssconverter.RssItem;

/**
 * Created by artem on 03.09.2017.
 */

public class ItemAdapter {

    public static List<Item> getItems(List<RssItem> rssItems) {
        ArrayList<Item> items = new ArrayList<>();
        for (RssItem rssItem : rssItems)
            items.add(getItem(rssItem));
        return items;
    }

    public static Item getItem(RssItem rssItem) {
        if (rssItem == null)
            return null;
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern("M/d/y k:m:s").toFormatter();
        long publishedDate = DateTime.parse(rssItem.getPublishDate(), formatter)
                .getMillis();
        String description = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            description = Html.fromHtml(rssItem.getDescription(), Html.FROM_HTML_MODE_LEGACY).toString();
        }
        else {
            description = Html.fromHtml(rssItem.getDescription()).toString();
        }
        return new Item(rssItem.getTitle(), description, publishedDate);
    }
}
