package com.kurantsou.softfxtest.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kurantsou.softfxtest.R;
import com.kurantsou.softfxtest.databinding.ActivityMainBinding;
import com.kurantsou.softfxtest.ui.adapter.TabsAdapter;
import com.kurantsou.softfxtest.ui.fragment.BaseListFragment;
import com.kurantsou.softfxtest.ui.fragment.NetworkFragment;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;
    private TabsAdapter tabsAdapter;
    private NetworkFragment mNetworkFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupTabs();
        setNetworkFragment();
        //subscribing to the NetworkFragment
        mNetworkFragment.setOnLoadListener(mOnLoadListener);
    }

    private NetworkFragment.OnLoadListener mOnLoadListener = new NetworkFragment.OnLoadListener() {
        @Override
        public void onLoadFinished() {
            mBinding.viewPager.setVisibility(View.VISIBLE);
            mBinding.tabLayout.setVisibility(View.VISIBLE);
            mBinding.pbLoading.setVisibility(View.GONE);
        }

        @Override
        public void onLoading() {
            mBinding.viewPager.setVisibility(View.GONE);
            mBinding.tabLayout.setVisibility(View.GONE);
            mBinding.pbLoading.setVisibility(View.VISIBLE);
        }
    };

    private void setNetworkFragment() {
        FragmentManager manager = getSupportFragmentManager();
        mNetworkFragment = (NetworkFragment) manager.findFragmentByTag(NetworkFragment.TAG);
        if (mNetworkFragment == null) {
            mNetworkFragment = NetworkFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(mNetworkFragment, NetworkFragment.TAG)
                    .commit();
        }
    }

    private void setupTabs() {
        tabsAdapter = new TabsAdapter(this, getSupportFragmentManager());
        mBinding.viewPager.setAdapter(tabsAdapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unsubscribing from networkFragment
        mNetworkFragment.setOnLoadListener(null);
    }
}
