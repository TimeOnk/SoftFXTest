package com.kurantsou.softfxtest.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.kurantsou.softfxtest.R;
import com.kurantsou.softfxtest.databinding.ListItemBinding;
import com.kurantsou.softfxtest.model.Item;

import java.util.List;

/**
 * Created by artem on 01.09.2017.
 */

public class ItemsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_VIEW_TYPE = 0;
    private static final int LOAD_VIEW_TYPE = 1;

    private List<Item> items;
    private OnLoadExtraItemsListener mListener;

    public ItemsListAdapter(List<Item> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder holder = null;
        switch (viewType){
            case ITEM_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.list_item, parent, false);
                holder = new ItemViewHolder(view);
                break;
            case LOAD_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.load_item, parent, false);
                holder = new LoadItemViewHolder(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position < items.size() && holder instanceof ItemViewHolder) {
            Item model = items.get(position);
            ((ItemViewHolder)holder).setModel(model);
        }
        else if (position == items.size() && mListener != null && holder instanceof LoadItemViewHolder){
                mListener.onExtraItemsNeeded();
        }
    }

    public void setOnLoadExtraItemsListener(OnLoadExtraItemsListener onLoadExtraItemsListener){
        mListener = onLoadExtraItemsListener;
    }

    @Override
    public int getItemViewType(int position) {
        return position < items.size() ? ITEM_VIEW_TYPE : LOAD_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size() + (mListener != null && mListener.isHasExtraItems() ? 1 : 0);
    }

    public void addItems(List<Item> itemsToAdd) {
        int insertIndex = items.size() + 1;
        items.addAll(itemsToAdd);
        notifyItemChanged(items.size());
        notifyItemRangeInserted(insertIndex, itemsToAdd.size());
    }

    public void addItem(Item item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }
    public void setItems(List<Item> items){
        this.items = items;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private ListItemBinding mBinding;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

        public void setModel(Item model) {
            mBinding.setModel(model);
        }
    }
    public class LoadItemViewHolder extends RecyclerView.ViewHolder{

        private ProgressBar progressBar;

        public LoadItemViewHolder(View itemView) {
            super(itemView);
            this.progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    public interface OnLoadExtraItemsListener{
        void onExtraItemsNeeded();
        boolean isHasExtraItems();
    }
}
