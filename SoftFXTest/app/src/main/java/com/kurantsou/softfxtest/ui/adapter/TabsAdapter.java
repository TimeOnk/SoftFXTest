package com.kurantsou.softfxtest.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kurantsou.softfxtest.R;
import com.kurantsou.softfxtest.ui.fragment.AnalyticsPageFragment;
import com.kurantsou.softfxtest.ui.fragment.NewsPageFragment;

/**
 * Created by artem on 01.09.2017.
 */

public class TabsAdapter extends FragmentPagerAdapter {

    private static final int TABS_COUNT = 2;
    private static final int NEWS_TAB_POSITION = 0;
    private static final int ANALICS_TAB_POSITION = 1;

private String[] tabsTitles;

    public TabsAdapter(Context context, FragmentManager fm) {
        super(fm);
        tabsTitles = context.getResources().getStringArray(R.array.tabs_names);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case NEWS_TAB_POSITION:
                fragment = NewsPageFragment.newInstance();
                break;
            case ANALICS_TAB_POSITION:
                fragment = AnalyticsPageFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return TABS_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabsTitles[position];
    }
}
