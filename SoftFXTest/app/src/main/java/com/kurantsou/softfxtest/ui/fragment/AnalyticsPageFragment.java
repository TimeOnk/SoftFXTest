package com.kurantsou.softfxtest.ui.fragment;

import com.kurantsou.softfxtest.db.DbConstants;
import com.kurantsou.softfxtest.model.Item;

import java.util.ArrayList;
import java.util.List;


public class AnalyticsPageFragment extends BaseListFragment {

    public AnalyticsPageFragment() {
    }


    public static AnalyticsPageFragment newInstance() {
        AnalyticsPageFragment fragment = new AnalyticsPageFragment();
        return fragment;
    }

    @Override
    protected String getItemsTypeName() {
        return DbConstants.TypesEntry.ANALYTICS_TYPE_NAME;
    }
}
