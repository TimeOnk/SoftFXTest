package com.kurantsou.softfxtest.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kurantsou.softfxtest.R;
import com.kurantsou.softfxtest.databinding.FragmentBaseListBinding;
import com.kurantsou.softfxtest.db.ItemsDbHelper;
import com.kurantsou.softfxtest.model.Item;
import com.kurantsou.softfxtest.ui.adapter.ItemsListAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseListFragment extends Fragment implements ItemsListAdapter.OnLoadExtraItemsListener{

    private static final int FIRS_PAGE_SIZE = 30;
    private static final int PAGE_SIZE = 10;

    private static final String PAGE_KEY = "page_number";
    private static final String IS_HAS_ELEMENTS_KEY = "is_has_elements";

    private FragmentBaseListBinding mBinding;
    private ItemsListAdapter adapter;
    protected ItemsDbHelper mDbHelper;

    private int page = 0;
    private boolean isHasElements = true;

    public BaseListFragment() {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PAGE_KEY, page);
        outState.putBoolean(IS_HAS_ELEMENTS_KEY, isHasElements);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper = new ItemsDbHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_base_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = DataBindingUtil.bind(view);
        if (savedInstanceState != null) {
            page = savedInstanceState.getInt(PAGE_KEY, 0);
            isHasElements = savedInstanceState.getBoolean(IS_HAS_ELEMENTS_KEY, true);
        }
        setupList();
    }

    protected void setupList() {
        adapter = new ItemsListAdapter(getLoadedItems());
        adapter.setOnLoadExtraItemsListener(this);
        RecyclerView recyclerView = mBinding.rvItemsList;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    private List<Item> getLoadedItems() {
        List<Item> items = mDbHelper.getItems(getItemsTypeName(), FIRS_PAGE_SIZE + page * PAGE_SIZE, 0);
        if (items.size() < FIRS_PAGE_SIZE + page * PAGE_SIZE)
            isHasElements = false;
        return items;
    }

    @Override
    public void onExtraItemsNeeded() {
        if (!isHasElements) return;
        List<Item> extraItems = mDbHelper.getItems(getItemsTypeName(), PAGE_SIZE, FIRS_PAGE_SIZE + PAGE_SIZE * page);
        page++;
        isHasElements = extraItems != null && !extraItems.isEmpty() && extraItems.size() == PAGE_SIZE;
        adapter.addItems(extraItems);
    }

    @Override
    public boolean isHasExtraItems() {
        return isHasElements;
    }

    protected abstract String getItemsTypeName();
}
