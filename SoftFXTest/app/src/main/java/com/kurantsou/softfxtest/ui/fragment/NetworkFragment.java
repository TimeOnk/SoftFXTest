package com.kurantsou.softfxtest.ui.fragment;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kurantsou.softfxtest.api.ApiProvider;
import com.kurantsou.softfxtest.db.DbConstants;
import com.kurantsou.softfxtest.db.DbInserter;
import com.kurantsou.softfxtest.db.ItemsDbHelper;
import com.kurantsou.softfxtest.model.Item;
import com.kurantsou.softfxtest.model.ItemAdapter;
import com.kurantsou.softfxtest.ui.widget.UpdateDateWidget;
import com.kurantsou.softfxtest.utils.NetworkUtils;
import com.kurantsou.softfxtest.utils.SharedPreferencesUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NetworkFragment extends Fragment {

    public static final String TAG = "NetworkFragment";

    private boolean isNewsLoading = false;
    private boolean isAnalyticsLoading = false;

    private OnLoadListener mListener;

    public NetworkFragment() {
    }

    public void setOnLoadListener(OnLoadListener listener) {
        mListener = listener;
        if (listener == null)
            return;
        if (isLoading())
            listener.onLoading();
        else
            listener.onLoadFinished();
    }

    public static NetworkFragment newInstance() {
        NetworkFragment fragment = new NetworkFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (NetworkUtils.isNetworkAvailable(getActivity()))
            updateItems();
        else
            Toast.makeText(getActivity(), "Network unavailable!", Toast.LENGTH_SHORT).show();
    }

    private void updateItems() {
        ApiProvider.RssFeedApi mApi = ApiProvider.getApiInstance();
        if (mListener != null)
            mListener.onLoading();
        isNewsLoading = true;
        mApi.getNewsFeed()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rssFeed -> {
                            fillDb(ItemAdapter.getItems(rssFeed.getItems()), DbConstants.TypesEntry.NEWS_TYPE_NAME);
                            Toast.makeText(getActivity(), "News updated", Toast.LENGTH_SHORT).show();
                            isNewsLoading = false;
                            notifyLoaded();
                        },
                        throwable -> onError(throwable),
                        () -> isNewsLoading = false);
        isAnalyticsLoading = true;
        mApi.getAnalyticsFeed()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rssFeed -> {
                            fillDb(ItemAdapter.getItems(rssFeed.getItems()), DbConstants.TypesEntry.ANALYTICS_TYPE_NAME);
                            Toast.makeText(getActivity(), "Analytics updated", Toast.LENGTH_SHORT).show();
                            isAnalyticsLoading = false;
                            notifyLoaded();
                        },
                        throwable -> onError(throwable),
                        () -> isAnalyticsLoading = false);
    }

    private void onError(Throwable throwable) {
        Log.e(TAG, "onError: ", throwable);
        Toast.makeText(getActivity().getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void notifyLoaded() {
        SharedPreferencesUtils.setLastUpdatedDate(getActivity(), System.currentTimeMillis());
        updateWidgets();
        if (!isLoading() && mListener != null)
            mListener.onLoadFinished();
    }
    //force updates all widgets
    private void updateWidgets(){
        Context context = getActivity();
        AppWidgetManager man = AppWidgetManager.getInstance(context);
        int[] ids = man.getAppWidgetIds(new ComponentName(context, UpdateDateWidget.class));
        Intent updateIntent = new Intent();
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(UpdateDateWidget.WIDGETS_TO_UPDATE_IDS, ids);
        context.sendBroadcast(updateIntent);
    }

    private void fillDb(List<Item> items, String itemsTypeName) {
        DbInserter.insertItems(getActivity(), items, itemsTypeName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    public boolean isLoading() {
        return isNewsLoading || isAnalyticsLoading;
    }

    public interface OnLoadListener {
        void onLoadFinished();
        void onLoading();
    }
}
