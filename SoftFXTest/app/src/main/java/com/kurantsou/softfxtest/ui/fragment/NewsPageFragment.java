package com.kurantsou.softfxtest.ui.fragment;


import com.kurantsou.softfxtest.db.DbConstants;
import com.kurantsou.softfxtest.model.Item;

import java.util.ArrayList;
import java.util.List;


public class NewsPageFragment extends BaseListFragment {

    public NewsPageFragment() {
    }

    public static NewsPageFragment newInstance() {
        NewsPageFragment fragment = new NewsPageFragment();
        return fragment;
    }

    @Override
    protected String getItemsTypeName() {
        return DbConstants.TypesEntry.NEWS_TYPE_NAME;
    }
}
