package com.kurantsou.softfxtest.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.kurantsou.softfxtest.R;
import com.kurantsou.softfxtest.utils.SharedPreferencesUtils;
import com.kurantsou.softfxtest.ui.activity.MainActivity;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;


/**
 * Implementation of App Widget functionality.
 */
public class UpdateDateWidget extends AppWidgetProvider {

    public static final int OPEN_APP_REQUEST_CODE = 0;

    public static final String WIDGETS_TO_UPDATE_IDS = "update_widgets_ids";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        long updatedDate = SharedPreferencesUtils.getLastUpdatedDate(context);
        if (updatedDate > 0) {
            DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                    .appendPattern("dd.MM.yyyy HH:mm")
                    .toFormatter();
            widgetText = formatter.print(updatedDate);
        }
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.update_date_widget);
        views.setTextViewText(R.id.appwidget_text, widgetText);

        Intent openAppIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, OPEN_APP_REQUEST_CODE, openAppIntent, 0);
        views.setOnClickPendingIntent(R.id.appwidgetContainer, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(WIDGETS_TO_UPDATE_IDS)){
            int[] ids = intent.getIntArrayExtra(WIDGETS_TO_UPDATE_IDS);
            onUpdate(context, AppWidgetManager.getInstance(context), ids);
        }
        else
            super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created

    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

