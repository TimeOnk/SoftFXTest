package com.kurantsou.softfxtest.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by artem on 03.09.2017.
 */

public class SharedPreferencesUtils {

    private static final String SHARED_PREFERANCES_NAME = "softFxShared";
    private static final String LAST_UPDATED_DATE = "last_updated";

    public static long getLastUpdatedDate(Context context){
        SharedPreferences prefs = getPreferences(context);
        return prefs.getLong(LAST_UPDATED_DATE, -1);
    }
    public static void setLastUpdatedDate(Context context, long lastUpdatedDate){
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putLong(LAST_UPDATED_DATE, lastUpdatedDate);
        editor.apply();
    }
    private static SharedPreferences getPreferences(Context context){
        return context
                .getApplicationContext()
                .getSharedPreferences(SHARED_PREFERANCES_NAME, Context.MODE_PRIVATE);
    }
}
